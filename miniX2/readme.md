
# MiniX2

RunMe (1): https://frejaspetersen.gitlab.io/aesthetic-programming/miniX2/ 

RunMe (2): https://frejaspetersen.gitlab.io/aesthetic-programming/miniX2pt2/

For this miniX I have prepared two versions since I've had many ideas and these only became several while I was working on the project. 

I have not finished a proper result as I was injured during the week and thereby I wasn't able to work on my projekt... But my thoughts behind this miniX were basically that an emoji for me is something I use to express my feelings and not something I would have to be able to relate to. 

I hereby had a thought to create an emoji, which expresses itself through words. 

![](minix21.png)

The first edition is a two-eyed emoji where the eyes should be filled into whole circles (imagine it like a clock) - and in this process it should aim to express different emotions. For example, as seen in the picture above - the emoji on this screenshot are a bit confused or tired. 

In addition, the 'mouth' is made up by a lot of words that express different emotions. For this I have considered two different options for action. 1) That it should be possible to 'stop' at one word and thereby have a smiley with one feeling. Or 2) that the words should run on repeat in a random pattern, and thus actually express many different emotions, but here leave it up to the recipient of the emoji to assess what feeling is intended for the message. 
I have not given the emoji any color, as the white and black colors were seen to be most neutral. 

![](minix22.png)
For my second edition of this miniX, I've programmed two emojis that can be moved from side to side. Here my plan was to first drop some eyes down, which should be able to grasp with the big emoji. These should be able to get stuck, after words should fall. Words that describe different feelings. And these should also be grasped. Which would provide something visual in the form of an emoji with the same concept as my first edition. 

I have given this project colors as purple and light purple as it gave something nice visually. In addition, I had an idea that the big emoji was one's sense outwardly and the little emoji was going to show what the sender feels inside. 

After reading the chapter "Modifying the Universal" in the book "Executing Practices" and watching the youtube video. I was introduced to the concept of 'Unicode'. I was a bit surprised at how emojis are really perceived and how many people see it, as something they think should be relatable. Which is also one of the reasons why Apple has developed, for example, the thumb-up emoji in several different skin tones. 

For this, as I said earlier, I wanted to step back and emphasize how I really think an emoji should be perceived and used. 
