let txt = ['happy', 'sad', 'surprised', 'angry', 'loving', 'confused', 'scared', 'frustrated', 'tired', 'delighted', 'pleased', 'grateful', 'joyful', 'inspired', 'wounded', 'relaxed'];


function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(8);
}

function draw() {
  background(255, 255, 255);
  fill(255);
  ellipse(355, 220, 300, 350);
  fill(255);
  strokeWeight(1);

  let txtSphere = random(txt);

  textStyle(ITALIC);
  textSize(25);
  fill(0);
  text(txtSphere, 300, 300);

  arc(400, 150, 100, 100, 80, HALF_PI);
  arc(300, 150, 100, 100, 50, HALF_PI);

}

//eyes
