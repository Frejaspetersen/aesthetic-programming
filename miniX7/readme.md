
# MiniX7

# Object abstraction		

![](miniX7.png)

Runme: https://frejaspetersen.gitlab.io/aesthetic-programming/miniX7/ 
# My program

At first I had many ideas about which games I would like to make. But as time went by I found that I had a very hard time programming a game. And when I searched for inspiration online - for how to program the games I found the codes very complex. So I ended up doing Daniel Shiffmans' 'snake game' (https://thecodingtrain.com/CodingChallenges/115-snake-game-redux.html).

Here I wished to incorporate a home page where you had to be guided on how to play the game and how to start. And after finishing the game, there should have been a button you could press to restart the game.

# Objects and their related attributes

Because it's an object-oriented programmed game, I've implemented a class with the purpose to encapsulate the idea of data and functionality into an object which in this case is the snake. In addition, I have implemented various functions, such as function foodLocation and function Keypressed to control how the game goes.

# Object-oriented programming (OOP) and the wider implications of abstraction 

In comparison to the tofu game, the snake game is also seen to be object oriented programming as the game is very organized and concrete despite the fact that objects are seen to be abstractions. In addition, the book (aesthetic programming) mentions that OOP is designed to reflect the way the world is organized and imagined, at least from the perspective of a computer programmer. It provides an understanding of the ways in which relatively independent objects work through their relationships with other objects. 

It is mentioned above that OOP is seen to be concrete despite the fact that objects are abstractions. In contrast there is OOO - object-oriented ontology. Which is a philosophical 'speculation' about how objects exist and interact. In short, the difference between these is that OOO rejects the idea that objects are made through the perception of the human subject, and promotes the idea that objects, whether human or non-human, are autonomous (page 160).

# My program in a cultural context... 

The book argues that this object-oriented modeling of the world is a social-technical practice, "compressing and abstracting relations operative that different scales of reality, composing new forms of agency." (page 161).

With the abstraction of relations, there is an interplay between abstract and concrete reality. I find it a bit difficult to compare the Snake game with this approach. However, it may be argued that it is a very abstract snake since the actual bars are a black line that gets bigger and bigger as it moves into the red boxes. 

