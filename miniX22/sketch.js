// state
let circleY = 0;

function draw() {
  createCanvas(windowWidth,windowHeight);
  background(255);

  fill(0);
  circle(200, circleY, 50);
  // modify state
  circleY = circleY + 2;
  //reset state
  if(circleY > height) {
    circleY = 0;
  }

  fill(0);
  circle(400, circleY, 20);
  // modify state
  circleY = circleY + 2;

  let leftWall = 100;
  let rightWall = 900;

  // xm is just the mouseX, while
  // xc is the mouseX, but constrained
  // between the leftWall and rightWall
  let xm = mouseX;
  let xc = constrain(mouseX, leftWall, rightWall);

  // Draw the walls.
  stroke(200);
  line(leftWall, 0, leftWall, height);
  line(rightWall, 0, rightWall, height);

  // Draw xm and xc as circles.
  noStroke();
  fill(150, 20, 149);
  ellipse(xm, 30, 70, 70); // Not Constrained
  fill(100, 100, 194);
  ellipse(xc, 350, 150, 150); // Constrained
}
