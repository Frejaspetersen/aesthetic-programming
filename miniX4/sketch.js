let button;
let txt = '0';

function addOne() {
 txt = +1;
  console.log(txt+1);
}

function setup() {
  createCanvas(600, 600);
  background(255);
  textSize(25);

  button = createButton('very bad');
    text(txt, 150, 40, 70, 80);
  button.position(40, 40);
  button.mousePressed(addOne);

  button = createButton('bad');
    text(txt, 150, 120, 70, 80);
  button.position(40, 120);
  button.mousePressed(addOne);

  button = createButton('neutral');
    text(txt, 150, 200, 70, 80);
  button.position(40, 200);
  button.mousePressed(addOne);

  button = createButton('good');
    text(txt, 150, 280, 70, 80);
  button.position(40, 280);
  button.mousePressed(addOne);

  button = createButton('very good');
    text(txt, 150, 360, 70, 80);
  button.position(40, 360);
  button.mousePressed(addOne);
}


//let val = random(255);
//background(val);
