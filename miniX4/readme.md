
# MiniX4

Runme: https://frejaspetersen.gitlab.io/aesthetic-programming/miniX4/ 

![](miniX4.png)

# Capturing emotions 

Why do we always have to capture everything and why do we always have the urge to compare ourselves with each other?  
Nowadays, most of it is about collecting data for the purpose of adapting such marketing in the form of online advertisements, emails, etc. to fit the target audience perfectly. So they are tempted to spend money.  
But is it only for good reasons when talking about data collection or what purposes is data collected for? All in all, it's all about increasing the productivity. Increasing the productivity of everyday life.  
  
Every day we experience that all data is collected about us. When we like posts on facebook and comments on photos on Facebook, when we shop online or when we click on any website someone or something are collecting data. Whether it's a good thing or a bad thing, I shouldn't be able to say.

# My program 

The idea was to make a program which one should be able to interact with. Then it should be able to provide knowledge of specific data. In this case, it provides knowledge of how many people have interacted with the program and how they like the program in terms of the options; “very bad, bad, neutral, good, very good”. 

I've made a program that allows to rate the program itself. I do not think that the program is the coolest program ever made. Probably most of all because it didn't turn out as supposed to.

But I think it helps to questioning how everything can be judged. As it says in the book; Aesthetic Programming, "The use of a single Like button provides a good example of how our feelings are captured. The aptly named company “Happy or Not” who produce push button technology and analytics software — the kind found in supermarkets for instance, with happy or sad faces — also provide feedback technologies for the workplace, as indicated by their strapline: “Creating happiness in every business, worldwide. (...)  It is fairly clear how the clicks serve the interests of platform owners foremost, and, as if to prove the point, Facebook, and Instagram have tested the idea of hiding the metrics on posts in order to shift attention to what they prefer to call “connecting people" — as if to prove their interests to be altruistic.” 

It has just become a thing that everything must be measured and weighed. I can't help out wondering about the last sentence of the quote above - that facebook and instagram have tested the idea of hiding the metrics on posts in order to shift attention to what they prefer to call “connecting people” — as if to prove their interests to be altruistic. Can we ever become alturistic when it comes to social media, including likes, shares and comments?..




