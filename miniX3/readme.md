
# MiniX3

RunMe: https://frejaspetersen.gitlab.io/aesthetic-programming/miniX3/

![](miniX3.png)

# My program 

For this MiniX we were asked to desig and programme a throbber. I have made a code that makes it possible to change the background color and in addition it is possible to change the color of the circle running around in a square. You do this by moving the mouse.  

My thought was to make a throbber which you can interact with. I associate a throbber with waiting time. And with this throbber, you can take advantage of the wait with a little fun interaction. 

Among other things, I have used the loop function to form the colors for the circle. First, I defined a 'let = color [0,0,0]'. This is usable later in the code where I've made three 'if-statements' which aim is to change the color slowly. The function is three if-statements, each ranging from 0-255 in gamut. It starts at 0 and when it reaches 255 it starts over.   

In addition, I have used the loop function to make the circle move around in the same square repeatedly – in a loop.  

Furthermore, I have worked with the button-function. I have used this to make it possible to change the background color. This is only an extra gimmick. I wanted to explore and learn how to make a button.  

The book describes the fact that beginning and ending a given process becomes a philosophical problem and also “The Computer as Time-Critical Medium”. For this Wolfgang Ernst clarifies the ontological importance of time to the computer to operate and perform tasks. He points to key issues of programmability, feedback, and recursion.

# Throbbers in everyday life  

During everyday life we are often presented with throbbers. I experience it, among other things, when I look at Instagram, receive and load a Snap, when I shop clothes online or if, for example, I stream movies on Netflix. 

Each throbber is different and some more obvious and prominent than others. I think a throbber aims to 'hide' what's happening in the software itself. For example, if a movie on Netflix stops loading, an icon appears showing a percentage depending on how far it is in the loading-process. This is intended to inform the user that something has happened. Here, a throbber acts as a kind of diversion for the software to work. I think it's generally among people that we quickly intend to think that something is broken or not working if we are not informed that the software is 'aware' that something is loading or else. 

 
