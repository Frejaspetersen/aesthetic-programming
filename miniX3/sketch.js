let circleX = 350;
let circleY = 200;
let xSpeed = 2;
let ySpeed = 2;
let color = [0,0,0];
let button;

function setup() {
  createCanvas(700, 700);
  frameRate(60);

  r = random(255);
  g = random(255);
  b = random(255);

  button = createButton("Change background");
  button.mousePressed(changeColor);
  button.position(280, 340);

  background(r,g,b);
}

function draw() {
  fill(color[0], color[1], color[2]);
  circle(circleX, circleY, 30);

  circleX = circleX + xSpeed;
  circleY = circleY + ySpeed;

  if(circleX < 200 || circleX > 500) {
    xSpeed = xSpeed * -1;
  }

  if(circleY < 200 || circleY > 500) {
    ySpeed = ySpeed * -1;
  }
}
function mouseMoved() {
  //Color changes randomly!:
  //  color[0] = random(255);
  //  color[1] = random(255);
  //  color[2] = random(255);

  //color changes slowly
  color[0] = color[0]+1;
  if (color[0] > 255) {
    color[1] = color[1]+1;
    color[0] = 255;
    if (color[1] > 255) {
      color[2] = color[2]+1;
      color[1] = 255;
      if (color[2] > 255) {
        color = [0,0,0];
      }
    }
  }


//  color[0] = color[0]+random(255);
//  color[1] = color[1]+random(255);
//  color[2] = color[2]+random(255);
//  if (color[0] > 255 ||
//    color[1] > 255 ||
//    color[2] > 255 ) {
//    color = [0,0,0];
//  }
}

function changeColor() {
	r = random(255);
	g = random(255);
	b = random(255);
  background(r,g,b);
}
