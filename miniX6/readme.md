
# MiniX6

Runme: https://frejaspetersen.gitlab.io/aesthetic-programming/miniX6/ 

![](miniX6.png)

I have chosen to work with my miniX4 partly because it was not sufficiently finished and partly because it is the minix I think has the best message to work on. 


Actually, I've changed the whole program. Lately I've been quite frustrated with how many negative vibes and offensive comments there are all over on social media. With that said, I would like to continue working on some kind of data capture. I wanted to make a programme which forces you to write something offensive and then you are asked if you are sure you want to publish and also if you would be able to say the same in real life.  

My program is more than just a code. Compared to my miniX1 e.g., it is a program that aims to initiate thoughts of those who see it. In addition, it provides for debate. The programme is based on a deeper problem in society. The word aesthetic is not taken literally in this context as such. On the other hand, this is a more political approach. Normally aesthetics is seen to be related to something that looks good – but here it is more related to what is politically correct and incorrect. 

As mentioned earlier, programming can initiate debates and also help to ask questions about various things. Programming can change digital behavior.

Critical Making can help to put a critical attitude towards something that is a habit/ something that for us is convenience. For example, users of digital platforms are mostly comfortable with googling the meaning of a word rather than looking in a dictionary. Here we do not think about what consequences it has when we enter the dictionary saying yes to five cookies and perhaps writing some information about ourselves. Critical making can put a focus on habits - habits which we have despite data capture.

Today, we depend on having to be part of the digital culture – in connection with work, study, hobbies, etc., it has been necessary for me to have facebook in order to be able to keep up and stay updated. 

Critical making can also focus on cultural values and create a cultural reevaluation. We can use technological development to create debate and highlight the values of society. 

"The concept of critical making has many predecessors, all of which start with the assumption that built technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture."
(Quote from the text; Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions, Matt Ratto & Garnet Hertz) 

