let x = 0;
let y = 0;
let spacing = 50;

function setup() {

  createCanvas(windowWidth, windowHeight);

  background(80, 31, 170);
}


  function draw() {
    stroke(25, 200, 220);
    if (random(1) < 0.10) {
    line(x, y, x+spacing, y+spacing);
  }
    else {
    line(x, y+spacing, x+spacing, y);

    }

      x+=10;
      if (x > width) {

        x = 0;
        y += spacing;
  }
}
