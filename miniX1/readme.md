# Mini X1

I've had trouble getting the link to work. It keeps saying "404 page you're looking for could not be found." 
I asked for help and was told it might not work sometimes so I had to wait and then try again. Then a fellow student assumed that a ".gitlab-ci.yml" file was missing in my folder, so this one I made - I think... But it still doesn't work for me. 
So here's a link I've taken from my browser: https://frejaspetersen.gitlab.io/aesthetic-programming/miniX1/ . I hope it works. Otherwise just let me know! (It's a piece you can interact with) 

![](minix1.png)

In this first mini X project I have produced a random piece of coding which you can interact with.  

I had some trouble getting started with my miniX1, so I went on youtube and watched slightly different videos with Dan Shiffman. These made it a little more manageable and helped me get started. I was inspired to make something with some colors and in addition I had a desire to make something which you could interact with.  

I started by making a background and then a circle. But after I'd experimented a little more with it, I ended up having a triangular figure.  

Hereafter I started experiencing with some colours. This turned out to make far better sense than I had first thought it would. I found this website; https://htmlcolorcodes.com/. With this I could easily get exactly the colours I wanted.  

Finally, I wanted to make something moveable and thereby make the piece something that you can interact with. Here I found a function "if-else" in p5.js under "References". With this I could change the colour with a click on the mouse.  

Then I also found the functions "mouseX" and "mouseY". I used these to make the triangle moveable. 	 

At first, I felt a little lost on how to begin with this assignment. I didn't really know what to do. But after watching videos on youtube and talking to the body-group, it gave me a better understanding and courage to code and where to start. In addition, I also experienced that I did'nt quite know what I was doing, but I managed to make something, and this gave me the desire to get better and accomplish more knowledge.  

The difference between coding and writing a text seems to me to be quite different in terms of curiosity and "experiences". By that I mean while I was coding I had more "’aha’-experiences" than if I were to write a text. I get very caught up in the fact that something visual comes out of what I'm doing. 

Programming is becoming more and more topical. The assigned reading helps me to understand the meaning between reading, writing and coding. And furthermore it hopefully helps me to reflect on the cultural and aesthetics dimensions of coding.  

I have always had an idea that coding did not - as such - have a deeper meaning, but by the fact that I’m already able to understand that coding is much more than just numbers, characters and letters it gives me a feeling to know much more and learn much more. 				 

					 
