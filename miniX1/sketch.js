function setup() {
  createCanvas(1500, 700);
  background(61,30,255);
}

function draw() {
  if (mouseIsPressed) {
    fill(20,132,30);
  } else {
    fill(30,255,248);
  }
  triangle(mouseX, mouseY,200, 300, 269, 120, 220, 230);
}
