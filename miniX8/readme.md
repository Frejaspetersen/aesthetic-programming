
# MiniX8 

RunMe: https://markusbjerremand1.gitlab.io/aesthetic-programming/MiniX8/

![](miniX8.png)

# Our Program - S1R1

For this week's miniX, we started out with the idea of creating a program inspired by a work we analyzed in Software Studies. A work called J3RR1 consisting of a computer which is being stress testing with the aim of questioning whether it is possible or not to feel empathy with a computer. 

In doing so, we made a program which, in the JSON-file, is divided into three arrays. The first array is called obliviousFriend, which is the phase where the computer sees itself as the user's 'friend'. At this stage, only positive things are said. The computer can feel self-awareness and it has control over the situation. The second array is called usedTool. During this phase, the computer is seen as being a tool we use. At this stage, the computer is not aware of its presence and furthermore, we imagine the fact that the computer is just a tool for us. There is still some amount of control but the statements at this stage are a lot less personal. Here, words such as; usable, created and programmed are used. The third and final stage is called awareSlave. During this phase, the computer is seen as a slave to the user. It feels used and, in this phase, it also becomes aware of its presence and the fact that it is overloaded and pushed to the limit. 

In each array there are three more phases. These are divided into; iFeel, iThink and iAm. The aim of these categories is to create a form of empathy and understanding of the computer's situation. To support this, a voiceover has been implemented to read the text aloud. This also helps to create empathy and familiarize ourselves with the computer's situation. 

Finally, after the computer has gone through the last stage, where it has been overloaded and now unable to work, a final stage occurs to express that it is all finished. The three words; iThink, iFeel and iAm occurs on the screen. 

# How it works 

First, we made a JSON-file. In this, a ‘main’ array holds three different element; the first being “state” : “obliviousFriend”, the secodin being “state” : “usedTool” and the last is “state” : “awareSlave”. These are three different states of “consciousness” that the program “experiences” throughout the execution of the program, going from a state of being oblivous to a state of being aware. Each of these states contain the three arrays mentioned in the "Our Program" section; iFeel, iThink, iAm. As the ‘state of consciousness’ increases, the program feels differently, thinks differently, and perceives itself differently.  

Our sketch.js file is where we utilized this JSON-file, loading it in through the variable ‘awareness’ in the preload()-function. Here we also load in a custom font, ‘CascadiaCode.ttf’, a text font used for programming that is used to inform the appearance of the text-elements throughout the program. The choice to use a programming font coincides with the idea, of the program typing- and talking directly to the user (or watcher) of the programme.  

In the setup()-function, a speech object from the p5.speech library is loaded with the variable ‘speech’.  
Furtheremore, the variable iterationState is set to equal 0; this variable indicates which state in the JSON-file that is so be accessed. The variable iterationState is set to equal a random floored number between 0 – 9. The same goes for iterationThink and iterationBe. This is due to the fact that there are 10 elements in each of these arrays in the first ‘state of consciousness’ (state[0], or rather, “state” : “obliviousFriend”).  

In the draw()-function, the variable state is declared to be equal to awareness.stateOfConsciousness. This creates a path to the array for the ‘state’-elements in the JSON. The variables ‘feeling’, ‘thinking’ and ‘being’ are declared and set to be equal to state[iterationState].iFeel/iThink/iAm. As explained earlier, iterationState indicates which ‘state’ that is to be accessed [0, 1, 2]. This creates a path to the arrays inside the state element. With iterationState being a variable, it can be changes by adding 1 to it’s current value (which is done through if-statements utilizing frameCount to make the value of iterationState change (which changes which ‘state’-element that is to accessed from the JSON). 
The text-element below draws the element that are retrieved from each of the three different arrays in the current ‘state’ that is accessed in the JSON-file. 
The if-statement below utilizes the frameCount% as a condition to ‘make something happen’ after x amount of time has passed since the programme began. With a frameRate set to 60, the ‘if (frameCount%600 == 0) means ‘each time 600 frames have passed’ equating to ‘every 10 seconds’ (as 600/60 is 10). 
Every 10 seconds, a new random number is chosen for the variables iterationFeel, iterationThink, and iterationBe – meaning that a new element from each of the three arrays in the current state-element will be randomly selected. Furthermore, every 10 seconds, the speak-object will read the randomly selected values out loud to the user of the programme. 

The rest of the if-statements declare, that when x amount of time have passed, the iterationState will either equate to 1 (state[1]) or 2(state[2]). Furtheremore, the variable ‘end’ is changed, as it indicates the value of the last element of the array. For each state, the length of the arrays get shorter as there are less elements in each array, with only 7 elements in each array in the last state.  
The last if statement’s condition is met when 3 minutes have passed since starting the programme. This will trigger the overload()-function, that clears the canvas. Now only a single text-element is shown, stating that ‘i feel, I think, I am’ - the programme is now aware of itself, as it has surpassed its highest ‘stateOfConsciousness’. 

# Analysis of our work  

In terms of appearance, our program is relatively simple. The deeper meaning is expressed in our code. The code elaborates on what the meaning of the program is. The section “Vocable Code” by Geoff Cox and Winnie Soon argues the fact that code is both script and performance (page 167). The visual part aims to express and give a sense of the purpose with the work - namely to question a computer's 'presence' and how it develops, just as humans do. Through this we see an exciting aspect in terms of the question how we humans can feel empathy with computers - just like J3RR1. The sound in our program is also intended to express the computer's 'stress level'. 

# Reflections on our work in terms of Vocable Code 

In relation to the Vocable Code, the section “Vocable Code” by Geoff Cox and Winnie Soon says that Vocable Code is also considered to be code work or code poetry. It encourages both the audience and the machine to speak the code out loud. In relation to our program, we have had a focus on making code poetry in the code itself. We’ve made software as art not software to make art. 

 

# References:  

Digicult.it, J3RR1: http://digicult.it/design/j3rr1-empathy-with-the-machine-interview-with-none-collective/  

Geoff Cox and Winnie Soon, “Vocable Code,” Aesthetic Programming, 165-186  
